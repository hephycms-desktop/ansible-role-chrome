Role Name
=========

Install Google Chrome.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.chrome

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
